package com.nbs.app.eduqadua.API;

/**
 * Created by Indhira Sasadhara on 12/12/2017.
 */

public class Getcomment {
    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    String created_at;
    String isi;
    String fullname;
}
