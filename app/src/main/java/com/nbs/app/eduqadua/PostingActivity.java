package com.nbs.app.eduqadua;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PostingActivity extends AppCompatActivity {


    public static final String URL = "https://whispering-cove-69856.herokuapp.com/";
    private ProgressDialog progress;


    @BindView(R.id.textIsi) EditText textIsi;

    @OnClick(R.id.btnPost) void posting(){
        //loading
        progress = new ProgressDialog(this);
        progress.setCancelable(false);
        progress.setMessage("Loading ...");
        progress.show();

        //mengambil data dari edittext
        String id_user = sharedPrefManager.getSP_Id();
        String isi = textIsi.getText().toString();
        String id_category = "1";
        String type = "ask";



        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        EduqaApi api = retrofit.create(EduqaApi.class);
        Call<Value> call = api.posting(id_user, id_category, isi, type);
        call.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(Call<Value> call, Response<Value> response) {
                progress.dismiss();
                String status = response.body().getStatus();
                if (status.equals("true")) {
                    Intent intent = new Intent(PostingActivity.this, MainActivity.class);
                    startActivity(intent);
                    Toast notif = Toast.makeText(PostingActivity.this, "Berhasil Update.", Toast.LENGTH_SHORT);
                    notif.show();
                }


            }

            @Override
            public void onFailure(Call<Value> call, Throwable t) {

            }
        });

    }

    SharedPrefManager sharedPrefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posting);
        ButterKnife.bind(this);

        sharedPrefManager = new SharedPrefManager(this);
    }
}
