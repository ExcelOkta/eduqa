package com.nbs.app.eduqadua;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Indhira Sasadhara on 12/12/2017.
 */

public class CommentViewHolder extends RecyclerView.ViewHolder {
    // ViewHolder akan mendeskripisikan item view yang ditempatkan di dalam RecyclerView.
    TextView tv1,tv2,tv3; //deklarasi textview
    ImageView imageView;  //deklarasi imageview

    public CommentViewHolder(View itemView) {
        super(itemView);

        tv1= (TextView) itemView.findViewById(R.id.nama_post);
        //menampilkan text dari widget CardView pada id daftar_judul
        tv2= (TextView) itemView.findViewById(R.id.isi_post);
        //menampilkan text deskripsi dari widget CardView pada id daftar_deskripsi
        tv3= (TextView) itemView.findViewById(R.id.waktu_post);
    }
}
