package com.nbs.app.eduqadua;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nbs.app.eduqadua.API.Getcomment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Indhira Sasadhara on 12/12/2017.
 */

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {


    private Context context;
    private List<Getcomment> results;

    public CommentAdapter(Context context, List<Getcomment> results) {
        this.context = context;
        this.results = results;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        ViewHolder holder = new ViewHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Getcomment data = results.get(position);
        holder.textViewNama.setText(data.getFullname());
        holder.textViewIsi.setText(data.getIsi());
        holder.textViewWaktu.setText(data.getCreated_at());
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.nama_post)
        TextView textViewNama;
        @BindView(R.id.isi_post)
        TextView textViewIsi;
        @BindView(R.id.waktu_post)
        TextView textViewWaktu;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
