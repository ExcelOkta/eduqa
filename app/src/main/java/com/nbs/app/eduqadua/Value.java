package com.nbs.app.eduqadua;

import com.nbs.app.eduqadua.API.Getcomment;
import com.nbs.app.eduqadua.API.Getcourselist;
import com.nbs.app.eduqadua.API.Getpost;

import java.util.List;

/**
 * Created by raqaelf on 16/11/17.
 */

public class Value {
    String status;
    String message;
    String user_id;
    String total;
    List<Getpost> data;
    List<Getcomment> comment;
    List<Getcourselist> courselist;

    public String getStatus() {
        return status;
    }

    public String getUserid() { return user_id; }

    public String getMessage() {
        return message;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<Getpost> getPost() {
        return data;
    }

    public List<Getcomment> getComment() { return comment; }

    public List<Getcourselist> getCourseList() { return courselist; }
}
