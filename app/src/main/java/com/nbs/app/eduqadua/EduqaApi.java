package com.nbs.app.eduqadua;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by raqaelf on 16/11/17.
 */

public interface EduqaApi {
    @FormUrlEncoded
    @POST("api/login")
    Call<Value> login(@Field("email") String email,
                      @Field("password") String password);
    @FormUrlEncoded
    @POST("api/register")
    Call<Value> regis(@Field("name") String nama,
                      @Field("email") String email,
                      @Field("password") String password);
    @FormUrlEncoded
    @POST("api/post/store")
    Call<Value> posting(@Field("id_user") String id_user,
                        @Field("id_category") String id_category,
                        @Field("isi") String isi,
                        @Field("type") String type);


    @GET("api/posts/5?")
    Call<Value> getPost(@Query("page") Integer page);
    @GET("api/comments/")
    Call<Value> getComment(@Query("id") Integer id);
}
