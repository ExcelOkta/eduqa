package com.nbs.app.eduqadua;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainLogin extends AppCompatActivity {

    public static final String URL = "https://whispering-cove-69856.herokuapp.com/";
    private ProgressDialog progress;

    @BindView(R.id.etUsername)
    EditText etUsername;
    @BindView(R.id.etPassword)
    EditText etPassword;





    @OnClick(R.id.btnLogin) void login(){
        //loading
        progress = new ProgressDialog(this);
        progress.setCancelable(false);
        progress.setMessage("Loading ...");
        progress.show();

        //mengambil data dari edittext
        String email = etUsername.getText().toString();
        String password = etPassword.getText().toString();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        EduqaApi api = retrofit.create(EduqaApi.class);
        Call<Value> call = api.login(email, password);
        call.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(Call<Value> call, Response<Value> response) {
                progress.dismiss();
                String status = response.body().getStatus();
                String idss = response.body().getUserid();

                if (status.equals("true")){
                    sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN,true);
                    sharedPrefManager.saveSPString(SharedPrefManager.SP_Id, idss);


                    Intent intent = new Intent(MainLogin.this, MainActivity.class);
                    startActivity(intent);
                } else if (status.equals("false")){
                    Toast notif = Toast.makeText(MainLogin.this, "username/password salah", Toast.LENGTH_SHORT);
                    notif.show();
                }
            }

            @Override
            public void onFailure(Call<Value> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(MainLogin.this, "Jaringan Error!", Toast.LENGTH_SHORT).show();

            }
        });

    }

    @OnClick(R.id.btnRegister) public void moveRegister()
    {
        Intent intent=new Intent(MainLogin.this, MainRegister.class);
        startActivity(intent);
    }


    SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        sharedPrefManager = new SharedPrefManager(this);
        if (sharedPrefManager.getSPSudahLogin()){
            startActivity(new Intent(MainLogin.this, MainActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }


    }
}
