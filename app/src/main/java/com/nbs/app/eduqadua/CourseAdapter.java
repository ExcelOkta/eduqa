package com.nbs.app.eduqadua;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nbs.app.eduqadua.API.Getcourselist;
import com.squareup.picasso.Picasso;

import java.lang.reflect.GenericArrayType;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Excel on 17/11/2017.
 */

public class CourseAdapter extends RecyclerView.Adapter<CourseAdapter.ViewHolder> {


    private Context context;
    private List<Getcourselist> results;

    public CourseAdapter(Context context, List<Getcourselist> results) {
        this.context = context;
        this.results = results;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        ViewHolder holder = new ViewHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Getcourselist data = results.get(position);
        holder.textViewNama.setText(data.getNama_course());
        holder.textViewIsi.setText(data.getDeskripsi_course());
        Picasso.with(context).load("http://i.imgur.com/DvpvklR.png").into(holder.imgCourse);
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.nama_course) TextView textViewNama;
        @BindView(R.id.deskripsi_course) TextView textViewIsi;
        @BindView(R.id.imgCourse) ImageView imgCourse;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
