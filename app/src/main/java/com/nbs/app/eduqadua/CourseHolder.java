package com.nbs.app.eduqadua;

import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by lenovo on 11/15/2017.
 */

public class CourseHolder extends RecyclerView.ViewHolder {
    // ViewHolder akan mendeskripisikan item view yang ditempatkan di dalam RecyclerView.
    TextView tv1,tv2; //deklarasi textview
    ImageView imgCourse;  //deklarasi imageview

    public CourseHolder(View itemView) {
        super(itemView);

        tv1 = (TextView) itemView.findViewById(R.id.nama_post);
        tv2 = (TextView) itemView.findViewById(R.id.isi_post);
        imgCourse = (ImageView) itemView.findViewById(R.id.imgCourse);
    }
}
