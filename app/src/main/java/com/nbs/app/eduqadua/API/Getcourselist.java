package com.nbs.app.eduqadua.API;

/**
 * Created by Excel on 18/11/2017.
 */

public class Getcourselist {
    String nama_course;
    String deskripsi_course;
    String image;

    public String getNama_course() {
        return nama_course;
    }

    public void setNama_course(String nama_course) {
        this.nama_course = nama_course;
    }

    public String getDeskripsi_course() {
        return deskripsi_course;
    }

    public void setDeskripsi_course(String deskripsi_course) {
        this.deskripsi_course = deskripsi_course;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
