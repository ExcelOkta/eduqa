package com.nbs.app.eduqadua;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainRegister extends AppCompatActivity {
    public static final String URL = "https://whispering-cove-69856.herokuapp.com/";
    private ProgressDialog progress;

    @BindView(R.id.etnama)
    EditText etnama;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etPassword)
    EditText etPassword;

    @OnClick(R.id.btnLogin) void moveLogin() {
        Intent intent= new Intent(MainRegister.this, MainLogin.class);
        startActivity(intent);
    }
    @OnClick(R.id.btnRegister) void regis() {
        //loading
        progress = new ProgressDialog(this);
        progress.setCancelable(false);
        progress.setMessage("Loading ...");
        progress.show();

        //mengambil data dari edittext
        String nama = etnama.getText().toString();
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        EduqaApi api = retrofit.create(EduqaApi.class);
        Call<Value> call = api.regis(nama, email, password);
        call.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(Call<Value> call, Response<Value> response) {
                progress.dismiss();
                String status = response.body().getStatus();
                if (status.equals("true")){
                    Intent intent= new Intent(MainRegister.this, MainLogin.class);
                    startActivity(intent);
                    Toast notif = Toast.makeText(MainRegister.this, "Berhasil Registrasi, silahkan login.", Toast.LENGTH_SHORT);
                    notif.show();
                }

            }

            @Override
            public void onFailure(Call<Value> call, Throwable t) {

            }
        });



    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

    }
}
