package com.nbs.app.eduqadua;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nbs.app.eduqadua.API.Getpost;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Excel on 17/11/2017.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {


    private Context context;
    private List<Getpost> results;

    public RecyclerViewAdapter(Context context, List<Getpost> results) {
        this.context = context;
        this.results = results;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        ViewHolder holder = new ViewHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Getpost data = results.get(position);
        holder.textViewNama.setText(data.getFullname());
        holder.textViewIsi.setText(data.getIsi());
        holder.textViewWaktu.setText(data.getCreated_at());
        holder.textViewiduser.setText(data.getId_user());
        holder.cvmain.setOnClickListener(new View.OnClickListener(position) {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, CommentActivity.class);
                i.putExtra(results.get(position).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.nama_post) TextView textViewNama;
        @BindView(R.id.isi_post) TextView textViewIsi;
        @BindView(R.id.waktu_post) TextView textViewWaktu;
        @BindView(R.id.iduser) TextView textViewiduser;
        @BindView(R.id.card_view) CardView cvmain;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
