package com.nbs.app.eduqadua;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nbs.app.eduqadua.API.Getpost;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    //Mendefinisikan variabel
    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private TextView userId;
    private Button navigasi4, nextpage, prevpage;
    private ImageView posting;
    private SwipeRefreshLayout swiperefresh;

    SharedPrefManager sharedPrefManager;

    //API
    public static Integer page = 1;
    public static final String URL = "https://whispering-cove-69856.herokuapp.com/";
    private List<Getpost> getposts = new ArrayList<>();
    private RecyclerViewAdapter viewAdapter;
    private EndlessRecyclerViewScrollListener scrollListener;

    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.progress_bar) ProgressBar progress_bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPrefManager = new SharedPrefManager(this);
        String userIds= sharedPrefManager.getSP_Id() ;
        userId = (TextView) findViewById(R.id.userId);
        userId.setText(userIds);
        ButterKnife.bind(this);


        //Get Posting
        viewAdapter = new RecyclerViewAdapter(this, getposts);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(viewAdapter);
        recyclerView.setSelected(true);
        loadNextDataFromApi(1);

        nextpage = (Button) findViewById(R.id.nextpage);
        prevpage = (Button) findViewById(R.id.prevpage);

        //Swipe to refresh
        swiperefresh = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                recyclerView.setVisibility(View.GONE);
                prevpage.setVisibility(View.GONE);
                loadNextDataFromApi(1);
                checkPage();
                swiperefresh.setRefreshing(false);

            }
        });

        checkPage();

        nextpage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerView.setVisibility(View.GONE);
                page = page+1;
                checkPage();
                loadNextDataFromApi(page);
            }
        });
        prevpage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerView.setVisibility(View.GONE);
                page = page-1;
                checkPage();
                loadNextDataFromApi(page);
            }
        });

//        scrollListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {
//            @Override
//            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
//                progress_bar.setVisibility(View.VISIBLE);
//                loadNextDataFromApi(page);
//            }
//        };
//        recyclerView.addOnScrollListener(scrollListener);
        //loadDataPost();
        //Button Posting
        posting = (ImageView)findViewById(R.id.btnPosting);
        posting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, PostingActivity.class);
            startActivity(intent);
            }
        });

        // Menginisiasi Toolbar dan mensetting sebagai actionbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Menginisiasi  NavigationView
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        //Mengatur Navigasi View Item yang akan dipanggil untuk menangani item klik menu navigasi
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                //Memeriksa apakah item tersebut dalam keadaan dicek  atau tidak,
                if (menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);
                //Menutup  drawer item klik
                drawerLayout.closeDrawers();
                //Memeriksa untuk melihat item yang akan dilklik dan melalukan aksi
                switch (menuItem.getItemId()) {
                    // pilihan menu item navigasi akan menampilkan pesan toast klik kalian bisa menggantinya
                    //dengan intent activity
                    case R.id.navigation1:
                        Intent intent = new Intent(MainActivity.this, KategoriActivity.class);
                        startActivity(intent);
                        return true;
                    case R.id.navigation2:
                        Intent course = new Intent(MainActivity.this, CourseActivity.class);
                        startActivity(course);
                        return true;
                    case R.id.navigation3:
                        Intent intents = new Intent(MainActivity.this, CourseActivity.class);
                        startActivity(intents);
                        return true;
                    case R.id.navigation4:
                        sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, false);
                        startActivity(new Intent(MainActivity.this, MainLogin.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
                        Toast.makeText(getApplicationContext(), "Logout", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.navigation5:
                        Toast.makeText(getApplicationContext(), "About telah dipilih", Toast.LENGTH_SHORT).show();
                        return true;
                    default:
                        Toast.makeText(getApplicationContext(), "Kesalahan Terjadi ", Toast.LENGTH_SHORT).show();
                        return true;
                }
            }
        });
        // Menginisasi Drawer Layout dan ActionBarToggle
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.closeDrawer) {
            @Override
            public void onDrawerClosed(View drawerView) {
                // Kode di sini akan merespons setelah drawer menutup disini kita biarkan kosong
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                //  Kode di sini akan merespons setelah drawer terbuka disini kita biarkan kosong
                super.onDrawerOpened(drawerView);
            }
        };
        //Mensetting actionbarToggle untuk drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        //memanggil synstate
        actionBarDrawerToggle.syncState();
        }

    private void checkPage() {
        if (page == 1) {
            prevpage.setVisibility(View.GONE);
        } else {
            prevpage.setVisibility(View.VISIBLE);
        }
    }

    private void loadNextDataFromApi(int page) {
        progress_bar.setVisibility(View.VISIBLE);
        //recyclerView.setVisibility(View.GONE);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        EduqaApi api = retrofit.create(EduqaApi.class);
        Call<Value> call = api.getPost(page);
        call.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(Call<Value> call, Response<Value> response) {
                progress_bar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                getposts = response.body().getPost();
                viewAdapter = new RecyclerViewAdapter(MainActivity.this, getposts);
                recyclerView.setAdapter(viewAdapter);
            }

            @Override
            public void onFailure(Call<Value> call, Throwable t) {

            }
        });
    }
}